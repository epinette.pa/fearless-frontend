window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingSpinner = document.querySelector('#loading-conference-spinner')

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      // Here, add the 'd-none' class to the loading icon
      loadingSpinner.classList.add('d-none');
      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');
    }

  });

  const formTag = document.getElementById('create-attendee-form');
  const successAlert = document.getElementById('success-message');
  formTag.addEventListener('submit', async event => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));
    const attendeeUrl = 'http://localhost:8001/api/conferences/1/attendees/'
    const fetchConfig = {
      method: 'post',
      body: json,
      header: {
        'Content-Type': 'application/json'
      },
    };
    try {
      const attendeeResponse = await fetch(attendeeUrl, fetchConfig);
      if (attendeeResponse.ok) {
        formTag.reset();
        const newAttendee = await attendeeResponse.json();
        console.log(newAttendee);
        // Here, add the 'd-none' class to the loading icon
        successAlert.classList.remove('d-none');
        // Here, remove the 'd-none' class from the select tag
        formTag.classList.add('d-none');
      }
    } catch (e) {
      console.error('Error while submitting the form', e);
    }
  });
