function createCard(title, description, pictureUrl, location, dateStart, dateEnd,) {
    const formattedStartDate = new Date(dateStart).toLocaleDateString('en-US', {
        month: 'numeric',
        day: 'numeric',
        year: 'numeric'
    });

    const formattedEndDate = new Date(dateEnd).toLocaleDateString('en-US', {
        month: 'numeric',
        day: 'numeric',
        year: 'numeric'
    });

    return `
        <div class="card shadow-lg">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${title}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${formattedStartDate} - ${formattedEndDate}</div>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Handle the case when the response is not okay (e.g., status code is not 200)
            const errorData = await response.json();
            console.error('Error:', errorData);
            // Perform specific error handling actions, such as displaying an error message to the user
            const errorContainer = document.getElementById('errorContainer');
            errorContainer.textContent = 'An error occurred while fetching the data. Please try again later.';
            errorContainer.classList.remove('d-none');
        } else {
            const data = await response.json();
            // Process the retrieved data when the response is okay

            const columnCount = 3; // Number of columns
            let columnIndex = 0;

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);

                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    // Process the retrieved data when the response is okay
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const dateStart = details.conference.starts;
                    const dateEnd = details.conference.ends;
                    const html = createCard(title, description, pictureUrl, location, dateStart, dateEnd);

                    const column = document.querySelectorAll('.col')[columnIndex];
                    column.innerHTML += html;

                    columnIndex = (columnIndex + 1) % columnCount; // Wrap around to the first column

                    // Add spacing between cards in the same column
                    const cards = column.querySelectorAll('.card');
                    cards.forEach(card => {
                        card.classList.add('mb-3'); // Add Bootstrap margin-bottom class
                    });
                }
            }
        }
    } catch (e) {
        // Handle any other errors that occurred during the asynchronous operations
        console.error('An error occurred:', e);
        // Perform specific error handling actions, such as displaying an error message to the user
        // Display error message
        const errorContainer = document.getElementById('errorContainer');
        errorContainer.textContent = 'An error occurred while fetching the data. Please try again later.';
        errorContainer.classList.remove('d-none');
    }

});
